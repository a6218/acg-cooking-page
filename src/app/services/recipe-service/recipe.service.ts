import { Injectable } from '@angular/core';
import { Ingredient } from 'src/app/models/ingredient.model';
import { Recipe } from 'src/app/models/recipe.model';
import { ShoppingListService } from '../shopping-list-service/shopping-list.service';
import { v4 as uuidv4 } from 'uuid';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipes$: BehaviorSubject<Recipe[]>;

  constructor(private shoppingListService: ShoppingListService) {
    this.recipes$ = new BehaviorSubject<Recipe[]>(this.recipes);
  }

  private recipes: Recipe[] = [
    this.createRecipe(
      'Pasta',
      'Pasta the La Neeeese',
      'https://runningonrealfood.com/wp-content/uploads/2021/02/Vegan-One-Pot-Pasta-Recipe.jpg',
      [
        this.shoppingListService.createIngredient('Banannas', 34),
        this.shoppingListService.createIngredient('Potatos', 12),
      ]
    ),
    this.createRecipe(
      'Lasagna',
      'La la la sag ma',
      'https://www.kindernetz.de/sendungen/schmecksplosion/1604999776975,image-knet-3460~_v-16x9@2dXL_-77ed5d09bafd4e3cf6a5a0264e5e16ea35f14925.jpg',
      [
        this.shoppingListService.createIngredient('Olives', 23),
        this.shoppingListService.createIngredient('Oranges', 4),
      ]
    ),
  ];

  getRecipes(): Recipe[] {
    return this.recipes.slice(); // returns a copy
  }

  getRecipe(id: number): Recipe {
    return this.recipes.find(
      (recipe) => recipe.id === id
    );
  }

  createRecipe(
    name: string,
    description: string,
    imagePath: string,
    ingredients: Ingredient[]
  ): Recipe {
    return new Recipe(
      uuidv4(),
      name,
      description,
      imagePath,
      ingredients
    );
  }

  addOrUpdateRecipe(recipe: Recipe) {
    if (!this.recipes.find(value => value.id === recipe.id)) {
      const newRecipe = this.createRecipe(
        recipe.name,
        recipe.description,
        recipe.imagePath,
        recipe.ingredients
      );
      this.recipes.push(newRecipe);
    }

    this.recipes = this.recipes.map(recipeItem => {
      if (recipeItem.id === recipe.id) {
        return {
          ...recipeItem,
          name: recipe.name,
          description: recipe.description,
          imagePath: recipe.imagePath,
          ingredients: recipe.ingredients
        };
      }

      return recipeItem;
    });

    this.recipes$.next(this.recipes);
  }

  deleteRecipe(recipeId: number) {
    this.recipes = this.recipes.filter(recipe => recipe.id !== recipeId);
    console.log('recipesAfterDelete: ', this.recipes);
    this.recipes$.next([...this.recipes]);
  }
}
