import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Ingredient } from 'src/app/models/ingredient.model';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  currentIngredients$: BehaviorSubject<Ingredient[]>;
  currentIngredientId$ = new BehaviorSubject<number>(null);

  ingredients: Ingredient[] = [
    this.createIngredient('Apples', 8),
    this.createIngredient('Tomatoes', 3),
  ];

  constructor() {
    this.currentIngredients$ = new BehaviorSubject<Ingredient[]>(this.ingredients);
  }

  createIngredient(name: string, amount: number): Ingredient {
    return new Ingredient(uuidv4(), name, amount);
  }

  getRecipe(id: number) {
    return this.ingredients.find(ingredient => ingredient.id === id);
  }

  addUpdateIngredient(name: string, amount: number) {
    if (!this.ingredients.find(value => value.name === name)) {
      this.ingredients.push(this.createIngredient(name, amount));
    }

    this.ingredients = this.ingredients.map(ingredient => {
      if (ingredient.name === name) {
        return { ...ingredient, name: name, amount: amount };
      }

      return ingredient;
    }).slice();

    this.currentIngredients$.next(this.ingredients);
  }

  addIngredients(recipeIngredients: Ingredient[]) {
    this.ingredients.push(...recipeIngredients);
    this.currentIngredients$.next([...this.ingredients]); // copy
  }

  deleteIngredient() {
    const recipeId = this.currentIngredientId$.getValue();
    this.ingredients = this.ingredients.filter(ingredient => ingredient.id !== recipeId);
    this.currentIngredients$.next([...this.ingredients]); // copy
  }
}
