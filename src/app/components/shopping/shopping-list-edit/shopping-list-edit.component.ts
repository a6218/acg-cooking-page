import { Component, ElementRef, EventEmitter, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { skip, Subscription } from 'rxjs';
import { ShoppingListService } from 'src/app/services/shopping-list-service/shopping-list.service';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.scss']
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {
  shoppingListForm: UntypedFormGroup;
  editMode = false;

  private onDestroy = new Subscription();

  constructor(
    private shoppingListService: ShoppingListService,
    private fb: UntypedFormBuilder,
  ) {
    this.shoppingListForm = this.fb.group({
      name: [null, [Validators.required]],
      amount: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    const ingredientIdSub = this.shoppingListService.currentIngredientId$
    .pipe(skip(1))
    .subscribe(recipeId => {
      this.editMode = true;
      const recipe = this.shoppingListService.getRecipe(recipeId);

      this.shoppingListForm.setValue({
        name: recipe.name,
        amount: recipe.amount,
      });
    });

    this.onDestroy.add(ingredientIdSub);
  }

  ngOnDestroy(): void {
    this.onDestroy.unsubscribe();
  }

  onSubmit(): void {
    if (!this.shoppingListForm.valid) {
      return;
    }

    const name = this.shoppingListForm.get('name').value;
    const amount = this.shoppingListForm.get('amount').value;
    this.shoppingListService.addUpdateIngredient(name, amount);

    this.onClear();
  }

  onDelete(): void {
    this.shoppingListService.deleteIngredient();
    this.onClear();
  }

  onClear(): void {
    this.shoppingListForm.reset();
    this.editMode = false;
  }
}
