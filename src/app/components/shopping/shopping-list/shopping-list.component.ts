import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Ingredient } from 'src/app/models/ingredient.model';
import { ShoppingListService } from 'src/app/services/shopping-list-service/shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {
  ingredients$: BehaviorSubject<Ingredient[]>;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit(): void {
    this.ingredients$ = this.shoppingListService.currentIngredients$;
  }

  onEdit(id: number): void {
    this.shoppingListService.currentIngredientId$.next(id);
  }
}
