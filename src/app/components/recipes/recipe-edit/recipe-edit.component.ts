import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Ingredient } from 'src/app/models/ingredient.model';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe-service/recipe.service';
import { ShoppingListService } from 'src/app/services/shopping-list-service/shopping-list.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss']
})
export class RecipeEditComponent implements OnInit {
  recipe: Recipe;
  editMode: boolean = false;
  recipeEditForm: UntypedFormGroup;
  previewImagePath = null;

  constructor(
    private recipeService: RecipeService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: UntypedFormBuilder,
    private shoppingListService: ShoppingListService,
  ) {
    this.initForm();
  }

  private initForm(): void {
    this.recipeEditForm = this.fb.group({
      id: [null],
      name: [null, [Validators.required]],
      description: [null, [Validators.required]],
      imagePath: [null, [Validators.required]],
      ingredients: this.fb.array([]),
    });
  }

  get ingredients(): FormArray {
    return this.recipeEditForm.get('ingredients') as FormArray;
  }

  private setIngredients() {
    if (this.recipe.ingredients !== undefined) {
      this.recipe.ingredients.forEach(ingredient => {
        const newIngredientGroup = this.fb.group({
          name: new FormControl(ingredient.name, Validators.required),
          amount: new FormControl(ingredient.amount, [
            Validators.required,
            Validators.pattern(/^[1-9]+[0-9]*$/)
          ]),
        })
        this.ingredients.push(newIngredientGroup);
      });
    }
  }

  private newIngredient(): FormGroup {
    const newIngredient = this.shoppingListService.createIngredient('', 0);
    return this.fb.group({
      id: newIngredient.id,
      name: new FormControl(null, Validators.required),
      amount: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^[1-9]+[0-9]*$/)
      ]),
    });
  }

  addIngredient(): void {
    this.ingredients.push(this.newIngredient());
  }

  removeIngredient(index: number): void {
    this.ingredients.removeAt(index);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
        const recipeId = params['id'];
        if (recipeId === undefined || this.recipeService.getRecipe(recipeId) === undefined) {
          return;
        }

        this.fillForm(recipeId);
        this.editMode = true;
      }
    );
  }

  fillForm(recipeId: number) {
    this.recipe = this.recipeService.getRecipe(recipeId);
    this.setIngredients();

    this.recipeEditForm.patchValue({
      id: this.recipe.id,
      name: this.recipe.name,
      description: this.recipe.description,
      imagePath: this.recipe.imagePath,
    });
  }

  onSubmit() {
    if (!this.recipeEditForm.valid) {
      return;
    }

    this.recipeService.addOrUpdateRecipe(this.recipeEditForm.value);
    this.onCancel();
  }

  onCancel() {
    this.router.navigate(['recipes']);
  }
}
