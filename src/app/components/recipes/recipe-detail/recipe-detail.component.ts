import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe-service/recipe.service';
import { ShoppingListService } from 'src/app/services/shopping-list-service/shopping-list.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  dropdown: boolean = false;

  constructor(
    private shoppingListService: ShoppingListService,
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        const recipeId = params['id'];

        this.recipe = this.recipeService.getRecipe(recipeId);
        if (this.recipe === undefined) {
          this.router.navigate(['recipes']);
        }
      }
    );
  }

  toggleDropdown(): void {
    this.dropdown = !this.dropdown;
  }

  onToShoppingList(): void {
    this.shoppingListService.addIngredients(this.recipe.ingredients);
  }

  onEdit(): void {
    this.router.navigate(['recipes/edit', this.recipe.id]);
  }

  onDelete(): void {
    this.recipeService.deleteRecipe(this.recipe.id);
    this.router.navigate(['recipes']);
  }
}
