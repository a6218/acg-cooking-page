import {
  ContentChild,
  Directive,
  Input,
  Renderer2
} from '@angular/core';
import { DropdownMenuDirective } from '../dropdown-menu/dropdown-menu.directive';

@Directive({
  selector: '[appDropdown]',
})
export class DropdownDirective {
  @Input('open') _open: boolean = false;
  @ContentChild(DropdownMenuDirective) private _menu: DropdownMenuDirective;

  constructor(private render: Renderer2) { }

  isOpen(): boolean {
    return this._open;
  }

  open(): void {
    if (!this._open) {
      this._open = true;
      this.render.addClass(this._menu.nativeElement, 'dropdown-show-items')
      this.render.removeClass(this._menu.nativeElement, 'dropdown-hide-items')
    }
  }

  close(): void {
    if (this._open) {
      this._open = false;
      this.render.addClass(this._menu.nativeElement, 'dropdown-hide-items')
      this.render.removeClass(this._menu.nativeElement, 'dropdown-show-items')
    }
  }

  toggle() {
    if (this.isOpen()) {
      this.close();
    } else {
      this.open();
    }
  }
}
