import { Directive, ElementRef } from '@angular/core';
import { DropdownDirective } from '../dropdown/dropdown.directive';

@Directive({
  selector: '[appDropdownToggle]',
  host: {
    'class': 'dropdown-toggle-button',
    '(click)': 'dropdown.toggle()',
  }
})
export class DropdownToggleDirective {
  nativeElement: HTMLElement
  constructor(public dropdown: DropdownDirective, elementRef: ElementRef) {
    this.nativeElement = elementRef.nativeElement;
  }
}
