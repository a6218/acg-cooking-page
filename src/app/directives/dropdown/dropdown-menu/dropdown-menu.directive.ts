import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropdownMenu]'
})
export class DropdownMenuDirective {
  nativeElement: HTMLElement;

  constructor(private elementRef: ElementRef<HTMLElement>) {
    this.nativeElement = elementRef.nativeElement;
  }
}
